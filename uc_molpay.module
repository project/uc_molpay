<?php
/**
 * Implements hook_help().
 */

define('UC_MOLPAY_PAYMENT_BASE_PATH', 'https://www.onlinepayment.com.my/NBepay/');

function uc_molpay_help($path, $arg) {
  switch ($path) {
    case 'admin/store/settings/payment/method/%':
      if ($arg[5] == 'molpay') {
        return '<p>' . t('To accept PayPal payments in molpay, please ensure that demo mode is disabled and your store currency is one of USD, AUD, CAD, EUR or GBP.') . '</p>';
      }
  }
}

/**
 * Implements hook_menu().
 */
function uc_molpay_menu() {
  $items = array();

  $items['cart/molpay/complete'] = array(
    'title' => 'Order complete',
    'page callback' => 'uc_molpay_complete',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'uc_molpay.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_init().
 */
function uc_molpay_init() {
  global $conf;
  $conf['i18n_variables'][] = 'uc_molpay_method_title';
  $conf['i18n_variables'][] = 'uc_molpay_checkout_button';
}

/**
 * Implements hook_ucga_display().
 */
function uc_molpay_ucga_display() {
  // Tell UC Google Analytics to display the e-commerce JS on the custom
  // order completion page for this module.
  if (arg(0) == 'cart' && arg(1) == 'molpay' && arg(2) == 'complete') {
    return TRUE;
  }
}

/**
 * Implementation of hook_form_FORM_ID_alter() for the uc_payment_methods_form.

 * Adds field management and ctools exportable links to the payment methods form.
 */
function uc_molpay_payment_form_uc_payment_methods_form_alter(&$form, &$form_state) {
  ctools_include('export');
  $methods = ctools_export_crud_load_all('uc_molpay_payment');
  foreach ($methods as $id => $method) {
    if (!empty($form['pmtable'][$id]["uc_payment_method_{$id}_checkout"])) {
      $form['pmtable'][$id]["uc_payment_method_{$id}_checkout"]['#title'] .= " ($method->type)";
    }
    if (!empty($form['pmtable'][$id]['settings']['#links'])) {
      $conditions = $form['pmtable'][$id]['settings']['#links']['conditions'];
      $ops = array('settings', 'conditions', 'delete', 'revert', 'clone', 'export', 'fields', 'display');
      $ops = array_combine($ops, array_fill(0, 8, TRUE));
      $ops['delete'] = !($method->export_type & EXPORT_IN_CODE);
      $ops['revert'] = ($method->export_type & EXPORT_IN_CODE) && ($method->export_type & EXPORT_IN_DATABASE);
      $ops = array_filter($ops);
      $form['pmtable'][$id]['settings']['#links'] = array();
      foreach (array_keys($ops) as $op) {
        if ($op == 'conditions') {
          $form['pmtable'][$id]['settings']['#links'][$op] = $conditions;
        }
        else {
          $form['pmtable'][$id]['settings']['#links'][$op] = array(
            'href' => UC_MOLPAY_PAYMENT_BASE_PATH . '/list/' . $id . '/' . $op,
            'title' => t($op),
          );
        }
      };
    }
  }
}

/**
 * Implements hook_uc_payment_method().
 *
 * @see uc_payment_method()
 */
function uc_molpay_uc_payment_method() {
  $path = base_path() . drupal_get_path('module', 'uc_molpay');
  $title = variable_get('uc_molpay_method_title', t('MolPay Payment:'));
  $title .= '<br />' . theme('image', array(
    'path' => drupal_get_path('module', 'uc_molpay') . '/images/molpay-logo.png',
    'attributes' => array('class' => array('uc-molpay-logo')),
  ));
  
  $payment_types = _uc_molpay_get_payment_types();

  $methods['uc_molpay'] = array(
  	'id' => 'Molpay',
    'name' => t('MolPay'),
    'title' => $title,
    'review' => variable_get('uc_molpay_payment', FALSE) ? t('Credit card/Bank Transfer') : t('Credit card/Bank Transfer'),
    'desc' => t('Redirect to Molpay to pay by credit card or bank transfer.'),
    'callback' => 'uc_payment_method_molpay',
    'redirect' => 'uc_molpay_form',
    'weight' => 3,
    'checkout' => TRUE,
    'no_gateway' => TRUE,
  );

  return $methods;
}

/**
 * Adds molpay settings to the payment method settings form.
 *
 * @see uc_molpay_uc_payment_method()
 */
function uc_payment_method_molpay($op, &$order, $form = NULL, &$form_state = NULL) {
  $path = base_path() . drupal_get_path('module', 'uc_molpay');
  switch ($op) {
    case 'cart-details':
      $build = array();
	  
if (variable_get('uc_molpay_payment', FALSE)) {
        if ($_SESSION['pay_method'] === NULL) {
          $type = 'credit';   // default to credit
        }
        else {
          $type = $_SESSION['pay_method'];
        }

        $selected = ' checked="checked"';

  // Fetch list of enabled payment methods from configuration.
  $enabled_payment_methods = _uc_molpay_get_payment_types();
  $payment_methods = array();
  if ($enabled_payment_methods) {
    foreach ($enabled_payment_methods as $key => $val) {
      if (in_array($key, variable_get('uc_molpay_enabled_payment_methods', array()))) {
        $payment_methods[$key] = $val;
      }
    }
  }

    $build['pay_method'] = array(
    '#type' => 'select',
    '#title' => t('Payment method'),
    '#default_value' => isset($_SESSION['pay_method']) ? $_SESSION['pay_method'] : 'CC',
    '#options' => $payment_methods,
    '#required' => TRUE,
    '#description' => t('Default is Credit Payment(Visa@mastercard), You can change payment method at the payment page'),
  );

        unset($_SESSION['pay_method']);
      }

      return $build;

    case 'cart-process':
      if (isset($form_state['values']['panes']['payment']['details']['pay_method'])) {
        $_SESSION['pay_method'] = $form_state['values']['panes']['payment']['details']['pay_method'];
      }
      return;

    case 'settings':

	  $form['uc_molpay_logo'] = array(
		'#markup' => '<div><img style="float:right;margin:0" src="'.$path.'/images/logo.png'.'" /></div>',
		);

      $form['uc_molpay_sid'] = array(
        '#type' => 'textfield',
        '#title' => t('Merchant ID'),
        '#description' => t('Your MolPay Merchant ID.'),
        '#default_value' => variable_get('uc_molpay_sid', ''),
        '#size' => 16,
      );

      $form['uc_molpay_secret_word'] = array(
        '#type' => 'textfield',
        '#title' => t('Verify Key'),
        '#description' => t('Your MolPay Verify Key.'),
        '#default_value' => variable_get('uc_molpay_secret_word', ''),
        '#size' => 50,
      );

      $form['uc_molpay_method_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Payment method title'),
        '#default_value' => variable_get('uc_molpay_method_title', t('MolPay Online Payment Gateway:')),
      );

	  $form['uc_molpay_payment'] = array(
        '#type' => 'fieldset',
        '#title' => t('Payment Types'),
      );
	  
	   $form['uc_molpay_payment']['info'] = array(
        '#type' => 'item',
        '#value' => t('Select the payment types to enable:'),
      );
	    foreach (_uc_molpay_get_payment_types() as $key => $val) {
        // credit card is enabled by default
        $form['uc_molpay_payment']['uc_molpay_payment_'. $key] = array(
          '#type' => 'checkbox',
          '#title' => _uc_molpay_payment_type_info($key, $val),
          '#default_value' => variable_get('uc_molpay_payment_'. $key, ($key == 'credit') ? TRUE : FALSE),
        );
      }
	  
      $form['uc_molpay_checkout_button'] = array(
        '#type' => 'textfield',
        '#title' => t('Order review submit button text'),
        '#description' => t('Provide molpay specific text for the submit button on the order review page.'),
        '#default_value' => variable_get('uc_molpay_checkout_button', t('Submit Order')),
      );

	   $form['uc_molpay_debug'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show debug info in the logs for MolPay. Useful for development or testing.'),
        '#default_value' => variable_get('uc_molpay_debug', FALSE),
      );

      return $form;
  }
}

/**
 * Form to build the submission to molpay.com.
 */
function uc_molpay_form($form, &$form_state, $order) {
	$_debug = variable_get('uc_molpay_debug', FALSE);
	
  $country = uc_get_country_data(array('country_id' => $order->billing_country));
  if ($country === FALSE) {
    $country = array(0 => array('country_iso_code_2' => 'MY'));
  }
$acc = variable_get('uc_molpay_sid', '');
  $data = array(
    'sid' => variable_get('uc_molpay_sid', ''),
    'amount' => uc_currency_format($order->order_total, FALSE, FALSE, '.'),
    'orderid' => $order->order_id,
    'pay_method' => isset($_SESSION['pay_method']) ? $_SESSION['pay_method'] : 'CC',
    'bill_desc' => drupal_substr(variable_get('site_name', 'Drupal Ubercart') . ' - Order no ' . $order->order_id,0, 100),
    'bill_name' => drupal_substr($order->billing_first_name . ' ' . $order->billing_last_name, 0, 128),
    'bill_email' => drupal_substr($order->primary_email, 0, 64),
    'returnurl' => url('cart/molpay/complete/' . uc_cart_get_id(), array('absolute' => TRUE)),
  );
  
  $data['vcode'] = md5($data['amount'] . $data['sid'] . $acc . $data['orderid'] . variable_get('uc_molpay_secret_word', ''));

  $i = 0;
  foreach ($order->products as $product) {
    $i++;
    $data['c_prod_' . $i] = $product->model . ',' . $product->qty;
    $data['c_name_' . $i] = $product->title;
    $data['c_description_' . $i] = '';
    $data['c_price_' . $i] = uc_currency_format($product->price, FALSE, FALSE, '.');
  }
  
  if (!isset($_SESSION['pay_method'])) {
    $type = 'credit';   // default to credit
  }
  else {
    $type = $_SESSION['pay_method'];
  }
  
  
    // additional post data
  if ($type == 'credit') {
    // TODO: currency options
    $data['country_id'] = $country[0]['country_iso_code_2'];
    $data['currency_id'] = 'rm';
  }

  $form['#action'] = _uc_molpay_post_url($type);

  foreach ($data as $name => $value) {
    $form[$name] = array('#type' => 'hidden', '#value' => $value);
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => variable_get('uc_molpay_checkout_button', t('Submit Order')),
  );

  return $form;
}

function _uc_molpay_payment_type_info($type, $name) {
  $output = '';

  $path = base_path() . drupal_get_path('module', 'uc_molpay') .'/images/';

  if ($type == 'credit') {
    $output .= '<img src="'. $path . $type .'.png" style="position: relative; top: 6px; margin-right: 5px;">';
    $output .= $name;
  }
  else if ($type == 'fpx') {
    $output .= '<img src="'. $path . $type .'.png" style="position: relative; top: 6px; margin-right: 5px;">';
    /*
	$output .= $name .' - Includes the following banks';
    $output .= '<br /><div style="padding-left: 4em;">';

    $banks = array('fpx-maybank2u', 'fpx-maybank2e', 'fpx-bankislam', 'fpx-cimbclicks', 'fpx-hongleong', 'fpx-pbebank', 'fpx-rhb');
    foreach ($banks as $bank) {
      $output .= '<img src="'. $path . $bank .'.png" style="position: relative; top: 6px; margin-right: 5px;">';
    }
    $output .= '</div>';
	*/
  }
  else {
    $output .= '<img src="'. $path . $type .'.png" style="position: relative; top: 6px; margin-right: 5px;">';
    $output .= $name;
  }

  return $output;
}


/**
 * Helper function to obtain molpay URL for a transaction.
 */
function _uc_molpay_post_url($type) {

  $root_url = UC_MOLPAY_PAYMENT_BASE_PATH;

  $acc = variable_get('uc_molpay_sid', '');

  switch ($type) {
    // Credit card
    case 'credit':
      $url = $root_url .'pay/'. $acc .'/';
      break;
	
    // Maybank2u Fund Transfer
	/*
    case 'mb2u':
      $url = $root_url .'pay/'. $acc .'/maybank2u.php';
      break;
	*/
	// MEPS FPX
    case 'fpx':
      $url = $root_url .'pay/'. $acc .'/fpx.php';
      break;

	/*
    // Mobile Money
    case 'mm-weblink':
      $url = $root_url .'pay/'. $acc .'/mobilemoney.php';
      break;

    // PosPay
    case 'pospay':
      $url = $root_url .'pay/'. $acc .'/pospay.php';
      break;

    // AmBank Online/AmOnline
    case 'amb-w2w':
      $url = $root_url .'pay/'. $acc .'/amb.php';
      break;

    // Alliance Online/iBayar
    case 'alb-onl':
      $url = $root_url .'pay/'. $acc .'/alb.php';
      break;

    // WebCash
    case 'webcash':
      $url = $root_url .'pay/'. $acc .'/webcash.php';
      break;

    // RHB Online
    case 'rhb-onl':
      $url = $root_url .'addPayment/rhb.php?merchantID='. $acc;
      break;

    // Mepscash Online
    case 'mepscash':
      $url = $root_url .'addPayment/mepscash.php?merchantID='. $acc;
      break;

    // China Online Bank
    case 'prc-ips':
      $url = $root_url .'addPayment/ips.php?merchantID='. $acc;
      break;
	*/

    default:
      $url = $root_url .'pay/'. $acc .'/';
      break;
  }

  return $url;
}

//Payment Types
function _uc_molpay_get_payment_types() {
  $payment_types = array(
    'credit'      => t('Credit card'),
    //'mb2u'        => t('Maybank2u Fund Transfer'),
    //'mm-weblink'  => t('Mobile Money'),
    //'pospay'      => t('PosPay'),
    'fpx'         => t('MEPS FPX'),
    //'amb-w2w'     => t('AmBank Online/AmOnline'),
    //'alb-onl'     => t('Alliance Online/iBayar'),
    //'webcash'     => t('WebCash'),
    //'rhb-onl'     => t('RHB Online'),
    //'mepscash'    => t('Mepscash Online'),
    //'prc-ips'     => t('China Online Bank'),
  );

  return $payment_types;
}
